#include <iostream>
#include <fstream>
#include <string>
#include <bitset>
#include <map>
using namespace std;



enum CommandTypes { A_COMMAND, C_COMMAND, L_COMMAND };
//parser class which paerses the code and tells which word or line has which commands/words/symbols etc
class Parser
{
public:
    
    fstream sourceCode;
    string line;
    CommandTypes command;


   
    Parser(string filename) {
        sourceCode.open(filename.c_str());
        if (sourceCode.is_open()) {
        }
        else {
            cout << "ERROR WHILE OPENEING FILE DURING PARSING" << endl;
        }
    }
    virtual ~Parser() {
    }
    void readLine() {
        getline(sourceCode, line);
        line = removeWhitespace(line);
    }
    bool hasMoreCommands() {
        if (getline(sourceCode, line)) {
            return advance();
        }
        else {
            return false;
        }
    }
    bool advance() {
        line = removeWhitespace(line);
        if (line.find("//") != string::npos) {
            line = line.substr(0, line.find("//"));
        }
        if (line.find("@") == 0) {
            command = A_COMMAND;
            return true;
        }
        else if (line.find("(") == 0) {
            command = L_COMMAND;
            return true;
        }
        else if (line.find("=") != string::npos || line.find(";") != string::npos) {
            command = C_COMMAND;
            return true;
        }
        else { 
            return hasMoreCommands();
        }
    }
    CommandTypes commandType() {
        return command;
    }
    string symbol() {
        if (line.find("@") != string::npos) {
            return line.substr(1);
        }
        else {
            return line.substr(1, line.length() - 2);
        }
    }
    string dest() {

        if (line.find("=") == string::npos) {
            return "null";
        }
        else {
            return line.substr(0, line.find("="));
        }
    }
    string comp() {
        int start = 0;
        int finish = line.length();
        if (line.find("=") != string::npos) {
            start = line.find("=") + 1;
        }
        if (line.find(";") != string::npos) {
            finish = line.find(";");
        }
        return line.substr(start, finish - start);
    }
    string jump() {
        if (line.find(";") == string::npos) {
            return "null";
        }
        else {
            return line.substr(line.find(";") + 1);
        }
    }

    string removeWhitespace(string str) {
        str.erase(remove(str.begin(), str.end(), ' '), str.end());

        return str;
    }

};

//Code Class which maintains and gives binary codes
class Code
{
public:
    map<string, string> comps;
    map<string, string> jumps;
    map<string, string> dests;
    Code() {
        comps["0"] = "0101010";
        comps["1"] = "0111111";
        comps["-1"] = "0111010";
        comps["D"] = "0001100";
        comps["A"] = "0110000";
        comps["!D"] = "0001101";
        comps["!A"] = "0110001";
        comps["-D"] = "0001111";
        comps["-A"] = "0110011";
        comps["D+1"] = "0011111";
        comps["A+1"] = "0110111";
        comps["D-1"] = "0001110";
        comps["A-1"] = "0110010";
        comps["D+A"] = "0000010";
        comps["D-A"] = "0010011";
        comps["A-D"] = "0000111";
        comps["D&A"] = "0000000";
        comps["D|A"] = "0010101";

        comps["M"] = "1110000";
        comps["!M"] = "1110001";
        comps["-M"] = "1110011";
        comps["M+1"] = "1110111";
        comps["M-1"] = "1110010";
        comps["D+M"] = "1000010";
        comps["D-M"] = "1010011";
        comps["M-D"] = "1000111";
        comps["D&M"] = "1000000";
        comps["D|M"] = "1010101";

        dests["null"] = "000";
        dests["M"] = "001";
        dests["D"] = "010";
        dests["MD"] = "011";
        dests["A"] = "100";
        dests["AM"] = "101";
        dests["AD"] = "110";
        dests["AMD"] = "111";

        jumps["null"] = "000";
        jumps["JGT"] = "001";
        jumps["JEQ"] = "010";
        jumps["JGE"] = "011";
        jumps["JLT"] = "100";
        jumps["JNE"] = "101";
        jumps["JLE"] = "110";
        jumps["JMP"] = "111";
    }

    virtual ~Code() {
    }

    string dest(string mnemonic) {
        return dests[mnemonic];
    }
    string comp(string mnemonic) {
        return comps[mnemonic];
    }
    string jump(string mnemonic) {
        return jumps[mnemonic];
    }
    
};


class SymbolTable
{
public:
    map<string, int> symbols;
    int curAddress;
    SymbolTable() {
        curAddress = 16;
        addEntry("SP", 0);
        addEntry("LCL", 1);
        addEntry("ARG", 2);
        addEntry("THIS", 3);
        addEntry("THAT", 4);

        addEntry("R0", 0);
        addEntry("R1", 1);
        addEntry("R2", 2);
        addEntry("R3", 3);
        addEntry("R4", 4);
        addEntry("R5", 5);
        addEntry("R6", 6);
        addEntry("R7", 7);
        addEntry("R8", 8);
        addEntry("R9", 9);
        addEntry("R10", 10);
        addEntry("R11", 11);
        addEntry("R12", 12);
        addEntry("R13", 13);
        addEntry("R14", 14);
        addEntry("R15", 15);

        addEntry("SCREEN", 16384);
        addEntry("KBD", 24576);

}
    void addEntry(string symbol, int address) {
        if (!contains(symbol)) {
            if (address == -1) {
                address = getNewAddress();
            }
            symbols[symbol] = address;
        }
    }
    bool contains(string symbol) {
        map<string, int>::const_iterator it = symbols.find(symbol);
        return it != symbols.end();
    }
    int getAddress(string symbol) {
        int address = symbols[symbol];
        return address;
    }
    int getNewAddress() {
        return curAddress++; 
    }
  
};


string removeWhitespace(string str) {
    str.erase(remove(str.begin(), str.end(), ' '), str.end());

    return str;
}

int main(int argc, char* argv[])
{
    string filename = "program.asm";
    cout << "Procession right now wait!!!\n";
    SymbolTable* table = new SymbolTable();

    Parser* parser = new Parser(filename); 
    Code* code = new Code();
    string finalString;
    while (parser->hasMoreCommands()) {
        if (parser->commandType() == A_COMMAND) {
             //Stores the found symbol
            string ori_symbol = parser->symbol();
            int symbol;
             //if symbol is a variable, get its address from the symbol table
            if (isalpha((int)ori_symbol.at(0))) {
                if (!table->contains(ori_symbol)) {
                    table->addEntry(ori_symbol, -1);
                }
                symbol = table->getAddress(ori_symbol);
            }
            else {
                symbol = atoi(ori_symbol.c_str());
            }
            bitset<15> bin_symbol(symbol);
            string val = bin_symbol.to_string();
            finalString = finalString + "0" + val + "\n";

        }
        if (parser->commandType() == C_COMMAND) {
            finalString = finalString + "111" + code->comp(parser->comp())
                + code->dest(parser->dest())
                + code->jump(parser->jump())
                + "\n";

            if (parser->jump() != "null") {
            }
            if (parser->dest() != "null") {
            }

        }
    }


    string outputFile = filename.substr(0, filename.length() - 4);
    outputFile = outputFile + ".hack";
    ofstream hackFile(outputFile);
    hackFile << finalString; 
    hackFile.close();
    cout << "Done!!!Check your file" << endl;
    return 0;
}


